
# makefile for memory.c
# Tyler Manifold & Isaac Murillo
# CSCI40300 HMW5

mmu: memory.c
	gcc -std=gnu99 $^ -o $@

run:
	./mmu

clean:
	rm mmu

