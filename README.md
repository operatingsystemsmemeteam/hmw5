
Tyler Manifold & Isaac Murillo

CSCI40300

HMW5

##Designing A Virtual Memory Manager

The goal of this project is to write a program that translates logical addresses to
physical addresses for a virtual address space of 2^16 (65,536) bytes. The program
should read logical addresses from a file and use a TLB and a page table to translate
the logical addresses to their corresponding physical addresses, and output the value
of the byte stored at the translated physical address.

###Specifics

Read a file containing several 32-bit integers representing logical addresses. However,
we only need to be concerned with 16-bit addresses, so the rightmost 16 bits of each 
address must be masked. These 16 bits are divided into an 8-bit page number and an
8-bit page offset. 

| bits | designation |
|------|-------------|
|31-16 | none	     |
|15-8  | page number |
|7-0   | offset      |

Other considerations:

* 2^8 entries in the page table
* Page size of 2^8 bytes
* 16 entries in the TLB
* Frame size of 2^8 bytes
* 256 frames
* Physical memory of 65,536 bytes (256 frames x 256-byte frame-size)

###Address translation

First, the page number is extracted from the logical address, and the TLB is consulted.
If TLB-hit, the frame number is obtained from the TLB. if TLB-miss, the page table must
be consulted. In this case, either the frame number is obtained from the page table or
a page fault occurs.

###Handling page faults

The program will implement demand paging as described in section 9.2, p401:
	
	"With demand-paged virtual memory, pages are loaded only when they are demanded
	during program execution. Programs that are never accessed are thus never loaded
	into physical memory"

The backing store is represented by the file `BACKING_STORE.bin`, a binary file of size
65,536 bytes. When a page-fault occurs, read in a 256-byte page from the `BACKING_STORE`
and store it in an available page frame in physical memory. Once the frame is stored
(and the page table and TLB have been updated), subsequent access to the faulting page
will be resolved by either the TLB or page table.

`BACKING_STORE.bin` must be treated as a random-access file so that certain positions
can be randomly seeked to for reading. Use C stdlib functions `fopen`, `fread`, `fseek`,
and `fclose`.
